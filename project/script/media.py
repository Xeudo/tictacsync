#!/bin/env python

import os
import ffmpeg
import logging
from thefuzz import fuzz

MINIMUM_LENGTH = 4 # Minimum duration of a media to be valid

class Media():
    """
    Class for a Media. A media contain all the information about a probe.
    """
    def get_sample_rate(self, audioStream):
        """
        Get the sample rate of the audioStream
        Args:
            audioStream (dict): the audio stream
        Returns:
            int: the sample rate of the audioStream
        """
        if not 'sample_rate' in audioStream:
            raise KeyError("No sample rate in the audio stream")
        sampleRate = int(audioStream['sample_rate'])
        logging.debug('Sample rate is: %i' %sampleRate)
        return sampleRate

    def has_audio(self, probe):
        """
        Check if the probe has audio or not

        Args:
           probe (Probe): probe of the given media

        Returns:
            boolean: true if there is audio and false if there is no audio
        """
        streams = probe['streams']
        codecs = [stream['codec_type'] for stream in streams]
        self.hasAudio = 'audio' in codecs
        return 'audio' in codecs
    
    def get_duration(self, audioStream):
        """
        Get the duration of the audio stream. 
        If theres a duration element in the prob it will return this attribute, otherwhise
        it will divide the amount of frame by the sample rate.

        Args:
            audioStream (dict): the audio stream

        Raises:
            Exception: If the duration or sample rate attribute are impossible to use

        Returns:
            float: the duration of the media in seconds
        """
        if 'duration' in audioStream:
            duration = float(audioStream['duration'])
        else:
            logging.error('Impossible to find duration from FFProbe')
            raise Exception('Impossible to find duration from FFProbe')
        logging.debug('Recording duration is: %f sec' %duration)
        self.duration = duration;
        return duration
    
    def get_audio_stream(self, probe):
        """ 
        Get the audio stream of the probe

        Args:
            probe (Probe): probe of the given media

        Raises:
            Exception: If there is multiple audio stream

        Returns:
            AudioStream: the audio stream of the probe
        """
        streams = probe['streams']
        audioStreams = [stream for stream in streams if stream['codec_type'] == 'audio']
        if len(audioStreams) > 1:
            raise Exception('ffprobe gave multiple audio streams?')
        audioStream = audioStreams[0]
        return audioStream
    
    def get_video_stream(self, probe):
        """ 
        Get the video stream of the probe

        Args:
            probe (Probe): probe of the given media

        Raises:
            Exception: If there is multiple video stream

        Returns:
            VideoStream: the video stream of the probe
        """
        streams = probe['streams']
        videoStreams = [stream for stream in streams if stream['codec_type'] == 'video']
        if len(videoStreams) > 1:
            raise Exception('ffprobe gave multiple video streams?')
        videoStream = videoStreams[0]
        return videoStream
    
    def prob_file(self, avPath):
        """
        Use ffmpeg to probe a file. Also check if the file contain all the element we need
        to ensure the compatibily of the software

        Args:
            avPath (string): the path of the file

        Returns:
            probe: probe of the given media
        """
        errorLog = ''
        probe = None
        try:
            probe = ffmpeg.probe(avPath)
        except:
            errorLog = 'is not recognized by FFProbe'
            logging.warning('"%s" %s' % (avPath, errorLog))
            raise OSError("File is not recognized by FFProbe")
        if probe is None:
            errorLog ='No FFProbe'
        elif probe['format']['probe_score'] < 99:
            errorLog = 'FFProbe score to low'
        elif not self.has_audio(probe):
            errorLog = 'No audio in file'
        elif self.get_duration(self.get_audio_stream(probe)) < MINIMUM_LENGTH:
            errorLog = 'File too short, %f s' %self.get_duration(self.get_audio_stream(probe))
        if errorLog == '':
            logging.debug('ffprobe found: %s' %probe)
        else:
            logging.warning('Recording init failed: %s'%errorLog)
        return probe;
    
    def is_mono(self, audioStream):
        """
        Check if the media is monaural. (Mono)

        Args:
            audioStream (dict): the audio stream

        Returns:
            boolean: if the audio stream is monaural
        """
        return audioStream['channels'] == 1

    def is_stereo(self, audioStream):
        """
        Check if the media is stereo

        Args:
            audioStream (dict): the audio stream

        Returns:
            boolean: if the audio stream is stereo
        """
        return audioStream['channels'] > 1
    
    def has_video(self, probe):
        """ Verify if the probe has a video stream

        Args:
            probe (Probe): probe of the given media

        Returns:
            boolean: if there is a video stream
        """
        videoStreams = [stream for stream in probe['streams'] if stream['codec_type'] == 'video']
        return len(videoStreams) > 0
        
    def check_for_multifiles(self, avPath, probe):
        """ Check if this is a multi-file recording. Look for other files
        with same size, same ffprobe dict and near same name.
        Sets sibling_filenames attribute.

        Args:
            avPath (string): the path of the file
            probe (Probe): probe of the given media

        Returns:
            array: Array containing all the siblings of the files
        """
        creationDate = os.path.getctime(avPath)
        filename = avPath.name
        directory = avPath.parent
        size = os.path.getsize(avPath)
        logging.debug('File : "%s" from directory %s of %f mb' % (filename, directory, round((size / (1024 * 1024)), 2),))
        filesWithSameSize = [file for file in os.listdir(directory) if os.path.getsize(directory/file) == size and file != filename]
        logging.debug('File with the same size: %s' %filesWithSameSize)
        return self.compare_files_with_same_size(filesWithSameSize, probe, directory, creationDate,filename)

    def compare_files_with_same_size(self, files, probe, directory, creationDate, filename):
        """
        Compare one file with a list of other files base on certain properties

        Args:
            files (array): The list of files to compare to
            probe (probe): The original file to compare
            directory (string): The directory of the original file
            creationDate (date): The creation date of the original file
            filename (string): the name of the original file to compare

        Returns:
            array: Array containing all the siblings of the files
        """
        siblings = []
        for file in files:
            logging.debug('Potential multi-file: "%s"' %file)
            isSameProb = self.compare_probe(probe, file, directory)
            logging.debug('Is both probe similar: %s' % isSameProb)
            isSimilarDate = abs(os.path.getctime(directory/file) - creationDate) < 60
            logging.debug('Is both date similar: %s' % isSimilarDate)
            isSimilarName = fuzz.ratio(filename, file) > 90
            logging.debug('Is both name similar: %s' % isSimilarName)
            if isSameProb and isSimilarDate and isSimilarName:
                siblings.append(file)
        if len(siblings) >= 1:
            logging.debug('This is a mutli-file : %s' % siblings)
        return siblings

    def compare_probe(self, orignal, suspected, directory):
        """
        Method use to compare two probe.

        Args:
            orignal (probe): Original probe
            suspected (string): avPath use to create a probe
            directory (string): directory of the prob

        Returns:
            boolean: true if both probe are identical
        """
        try:
            probe = ffmpeg.probe(directory/suspected)
        except:
            errorLog = 'is not recognized by FFProbe'
            logging.warning('"%s" %s' % (suspected, errorLog))
            return False
        probe['format']['filename'] = ''
        orignal['format']['filename'] = ''
        return probe == orignal
    
    def is_multifiles(self, siblings):
        """
        Check if the media has siblings

        Args:
            siblings (array): an array containing all the siblings of the file

        Returns:
            boolean: if the media as at least one siblings
        """
        if siblings is None: 
            return False
        return len(siblings) >= 1

    def show_info(self):
        """
        Print the current info about the media and log the info about the media
        """
        message = "File : %s\nSample rate: %i\nIs Mono: %s\nIs Stereo: %s\nHas video: %s\nOnly audio: %s\nIs multi-files: %s\nProbe : %s\n" % (self.path, self.sampleRate, self.isMono, self.isStereo, self.hasVideo, self.onlyAudio, self.isMultiFile, self.probe)
        logging.debug(message)
        print(message)
    
    def __init__(self, avPath):
        """
        Constructor of the Media
        Set filename string and check if file exists, does not read
        any media data right away but uses ffprobe to parses the file and
        sets probe attribute. 
        Logs a warning if ffprobe cant interpret the file or if file
        has no audio; if file contains audio, instantiates a Decoder objec
        Args:
            avPath (string): path of the given media
        """
        self.initialize_variables()
        logging.debug('Initializing Media object: "%s" from directory : %s' % (avPath.name, avPath.parent))
        self.path = avPath
        self.probe = self.prob_file(avPath)
        self.audioStream = self.get_audio_stream(self.probe)
        self.sampleRate = self.get_sample_rate(self.audioStream)
        self.isMono = self.is_mono(self.audioStream)
        self.isStereo = self.is_stereo(self.audioStream)
        self.hasVideo = self.has_video(self.probe)
        self.onlyAudio = not self.has_video
        if (self.hasVideo):
            self.videoStream = self.get_video_stream(self.probe)
        if (not self.probe == None):
            self.siblings = self.check_for_multifiles(avPath, self.probe)
            self.isMultiFile = self.is_multifiles(self.siblings) 
    
    def initialize_variables(self):
        """
        Initialize the variable to None.
        """
        logging.debug("Initializing Media variables")
        self.probe = None
        self.hasAudio = None
        self.duration = None
        self.sampleRate = None
        self.audioStream = None
        self.isMono = False
        self.isStereo = False
        self.onlyAudio = True
        self.hasVideo = False
        self.siblings = []