#!/usr/bin/env python3
import pathlib
import unittest
import ffmpeg
from media import Media

class TestMedia(unittest.TestCase):
    """ Class containing all the test for a Media

    Args:
        unittest (class): unnitest package
    """
    def test_has_audio(self):
        """
        Test use to validate that the file has audio.
        """
        audio = ffmpeg.probe(pathlib.Path('sample\canon.wav'))
        self.assertTrue(Media.has_audio(self,audio))

    def test_has_no_audio(self):
        """
        Test use to validate that the file has no audio
        """
        noAudio = ffmpeg.probe(pathlib.Path('sample\empty.m4v'))
        self.assertFalse(Media.has_audio(self,noAudio))
        
    def test_sample_rate(self):
        """
        Test use to retrieve the sample rate
        """
        probe =  ffmpeg.probe(pathlib.Path('sample\canon.wav'))
        audioStream = Media.get_audio_stream(self,probe)
        self.assertEqual(48000, Media.get_sample_rate(self,audioStream))

    def test_no_sample_rate(self):
        """
        Test to ensure there is an exception if no sample rate is present in the audio stream
        """
        probe =  ffmpeg.probe(pathlib.Path('sample\canon.wav'))
        audioStream = Media.get_audio_stream(self,probe).pop('sample_rate')
        with self.assertRaises(KeyError):
            Media.get_sample_rate(self, audioStream)
            
    def test_get_duration(self):
        """
        Test to retrieve the duration of a media
        """
        probe =  ffmpeg.probe(pathlib.Path('sample\canon.wav'))
        audioStream = Media.get_audio_stream(self,probe)
        self.assertEqual(18.418396, Media.get_duration(self, audioStream))

    def test_get_duration_without_duration(self):
        """
        Test to ensure there is an exception if no duration is present in the audio stream
        """
        probe =  ffmpeg.probe(pathlib.Path('sample\sample.m4v'))
        audioStream = Media.get_audio_stream(self,probe)
        del audioStream['duration']
        with self.assertRaises(Exception):
            Media.get_duration(self, audioStream)
            
    def test_prob_file_failed(self):
        """
        Test to ensure there is an exception if the probe failed
        """
        with self.assertRaises(OSError):
            Media.prob_file(self, "null")

    def test_is_mono(self):
        """
        Test to verify if the software detect monaural media
        """
        mono = ffmpeg.probe(pathlib.Path('sample\mono.wav'))
        audioStream = Media.get_audio_stream(self,mono)
        self.assertTrue(Media.is_mono(self, audioStream))

    def test_is_stereo(self):
        """
        Test to verify if the software detect a stereo media
        """
        stereo = ffmpeg.probe(pathlib.Path('sample\canon.wav'))
        audioStream = Media.get_audio_stream(self,stereo)
        self.assertTrue(Media.is_mono(self, audioStream))
    
    def test_has_video(self):
        """
        Test to verify if a file has video
        """
        video = ffmpeg.probe(pathlib.Path('sample\sample.m4v'))
        self.assertTrue(Media.has_video(self,video))
        
    def test_has_no_video(self):
        """
        Test to verify if a file has no video
        """
        video = ffmpeg.probe(pathlib.Path('sample\mono.wav'))
        self.assertFalse(Media.has_video(self,video))
    
    def test_is_multifiles(self):
        """
        Test to verify if the media has multi files
        """
        self.assertTrue(Media.is_multifiles(self,['P4CH003M.wav']))
    